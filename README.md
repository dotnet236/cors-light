__CorsLight__

Rails engine which proxies a url and adds cross origin resource sharing headers to the response

__How To Run Tests__

* Start the dummy project rails server
    1. cd test/dummy
    2. rails s -p 2029 -d
* Run rake *From the projects root
    4. rake

This project rocks and uses MIT-LICENSE.
