require 'action_dispatch'
require_relative 'open_uri_extensions/uri_origin'

class ActionDispatch::Response

  def allow_referrer_access_control
    begin
      referrer = URI.parse request[:referrer_url]
    rescue URI::InvalidURIError => e
      raise "Request referrer, #{request[:referrer_url]}, could not parsed as a valid URI.
            Internal Exception: #{e}"
    end

    headers['Access-Control-Allow-Origin'] = referrer.origin
    headers['Access-Control-Allow-Methods'] = 'GET'
    headers['Access-Control-Allow-Credentials'] = 'true'
  end

end
