module CorsLight
  THIRTY_SECONDS = 30
  DEFAULT_OPTIONS = {
    proxy_local_assets: false,
    proxy_read_timeout: THIRTY_SECONDS,
    application_url: nil,
    proxy_whitelist: []
  }

  class Engine < Rails::Engine
    isolate_namespace CorsLight

    initializer 'cors_light.set_default_configuration', after: :load_environment_config do |app|
      CorsLight::DEFAULT_OPTIONS.keys.each do |key|
        Rails.configuration.send("#{key}=", CorsLight::DEFAULT_OPTIONS[key]) if !Rails.configuration.respond_to? key
      end
    end

    initializer 'cors_light.validate_configuration', after: 'cors_light.set_default_configuration' do |app|
      if !Rails.configuration.proxy_local_assets and Rails.configuration.application_url.to_s == ''
        raise "No application_url specified when proxy_local_assets is set to true.
               Set Rails.configuration.proxy_local_assets = false"
      end
    end
  end
end
