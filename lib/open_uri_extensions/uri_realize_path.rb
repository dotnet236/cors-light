require 'open-uri'

module URI
  def self.realize_path(url)
    uri = parse(url)
    path = replace_double_slashes uri.path
    uri.path = ''
    collapse_relative_paths uri.to_s, path
  end

  private
  def self.replace_double_slashes(path)
    path = path.gsub(/^[\/]*/, '/') if path.start_with? '/'
    path
  end

  def self.collapse_relative_paths(path1, path2)
    URI.join(path1, path2).to_s
  end
end
