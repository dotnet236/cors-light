require 'open-uri'

module URI
  def self.strip_protocol(url)
    URI.parse(URI.encode(url[url.index('/')..-1])).to_s
  end
end
