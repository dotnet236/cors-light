require 'open-uri'

module URIOrigin
  def origin
    result = "#{scheme}://#{host}"

    if !default_port?
      result = "#{result}:#{port}"
    end

    result
  end

  private
  def default_port?
    default_port == port
  end
end

class URI::HTTP
  include URIOrigin
end

class URI::HTTPS
  include URIOrigin
end
