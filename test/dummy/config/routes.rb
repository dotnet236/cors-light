Rails.application.routes.draw do
  mount CorsLight::Engine => "/cors_light", as: 'cors_light_engine'

  root to: 'cors_light#index'
end
