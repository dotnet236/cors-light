require 'spec_helper'

describe 'CorsLight' do

  before :all do
    @reference_image = 'https://imgssl.constantcontact.com/galileo/images/reference/dont_open.gif'
    @no_exist_reference_image = 'https://imgssl.constantcontact.com/galileo/images/reference/dont_open_because_i_dont_exist.gif'
  end

  def cors_light_url(url_to_proxy, referrer_url)
    "http://localhost:2029/cors_light?asset_url=#{url_to_proxy}&referrer_url=#{referrer_url}"
  end

  content_type = 'image/gif'
  referrer_url = 'http://threeparsecsfromtrantor.mule'

  context '#proxy an unaccessible url that contains a space' do
    it 'should return a 404, not a 500' do
      image_url = "http://pleasedontexist.com/folder%20with%20spaces/dont_open_because_i_dont_exist.gif"
      proxy_url = cors_light_url image_url, referrer_url

      begin
        RestClient.get proxy_url, 'content-type' => content_type
        raise 'Image with an invaid asset_url specified was proxied through the cors light controller.  This is a problem'
      rescue RestClient::Exception => e
        e.http_code.should == 404
      end
    end
  end

  context '#proxy with a default port' do
    before :each do
      image_url = @reference_image
      proxy_url = cors_light_url image_url, 'http://normalsite.com'
      @response = RestClient.get proxy_url, 'content-type' => content_type
    end

    it 'should include no port in the access control header' do
      @response.headers[:access_control_allow_origin].should_not match ':80$'
    end
  end

  context '#proxy with a non-default port' do
    before :each do
      image_url = @reference_image
      proxy_url = cors_light_url image_url, 'http://sillysite.com:3001'
      @response = RestClient.get proxy_url, 'content-type' => content_type
    end
    it 'should include the port in the access control header' do
      @response.headers[:access_control_allow_origin].should match ':3001$'
    end
  end

  context '#proxy with a referrer containing a path and query string' do
    before :each do
      image_url = @reference_image
      proxy_url = cors_light_url image_url, 'http://getyourgrooveon.com/test?name=value'
      @response = RestClient.get proxy_url, 'content-type' => content_type
    end

    it 'should strip path and query string' do
      @response.headers[:access_control_allow_origin].should == 'http://getyourgrooveon.com'
    end
  end

  context '#proxy on a sourceable image' do

    before :each do
      image_url = @reference_image
      proxy_url = cors_light_url image_url, referrer_url
      @response = RestClient.get proxy_url, 'content-type' => content_type
    end

    it 'should return 200' do
      @response.code.should == 200
    end

    it 'should return that image' do
      @response.body.length == 978212
    end

    it 'should set the correct Access-Control-Allow headers' do
      headers = @response.headers
      headers[:access_control_allow_origin].should == referrer_url
      headers[:access_control_allow_methods].should == 'GET'
      headers[:access_control_allow_credentials].should == 'true'
    end

    it 'should set the content-type header relative to the image requested' do
      @response.headers[:content_type].should == content_type
    end
  end

  context '#proxy on a sourceable image with no referrer_url parameter specified' do

    it 'should return 404' do
      image_url = @no_exist_reference_image
      proxy_url = cors_light_url image_url, ''

      begin
        RestClient.get proxy_url,  'content-type' => content_type
        raise 'Image without a referrer specified was proxied through the cors light controller.  This is a problem'
      rescue RestClient::Exception => e
        e.http_code.should == 404
      end
    end

  end

  context '#proxy with no asset_url parameter specified' do

    it 'should return 404' do
      image_url = @no_exist_reference_image
      proxy_url = cors_light_url '', referrer_url

      begin
        RestClient.get proxy_url, 'content-type' => content_type
        raise 'Image without an asset_url specified was proxied through the cors light controller.  This is a problem'
      rescue RestClient::Exception => e
        e.http_code.should == 404
      end
    end

  end

  context '#proxy with an invalid asset_url parameter specified' do

    it 'should return 404' do
      image_url = "http:/assets/test/dont_open_because_i_dont_exist.gif"
      proxy_url = cors_light_url image_url, referrer_url

      begin
        RestClient.get proxy_url, 'content-type' => content_type
        raise 'Image with an invaid asset_url specified was proxied through the cors light controller.  This is a problem'
      rescue RestClient::Exception => e
        e.http_code.should == 404
      end
    end

  end

  context '#proxy on a sourceable image' do

    before :each do
      image_url = @reference_image
      proxy_url = cors_light_url image_url, referrer_url
      @response = RestClient.get proxy_url,  'content-type' => content_type
    end

    it 'should set the content-type header based on the urls extension' do
      @response.headers[:content_type].should == content_type
    end

  end

  context '#proxy on an image that cannot be found' do

    it 'should return 404' do
      image_url = @no_exist_reference_image
      proxy_url = cors_light_url image_url, referrer_url

      begin
        RestClient.get proxy_url, 'content-type' => content_type
        raise 'Invalid image was proxied through the cors light controller.  This is a problem'
      rescue RestClient::Exception => e
        e.http_code.should == 404
      end
    end

  end

end
