ENV["RAILS_ENV"] ||= 'test'
ENV['BUNDLE_GEMFILE'] ||= File.expand_path('../../Gemfile', __FILE__)

require File.expand_path("../../config/environment", __FILE__)

require 'bundler/setup' if File.exists?(ENV['BUNDLE_GEMFILE'])
require 'rspec/rails'
require 'rspec/autorun'
require 'open-uri'
require 'rest-client'

RSpec.configure do |config|
  config.mock_with :mocha
end
