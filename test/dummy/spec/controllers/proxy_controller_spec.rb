require_relative './../spec_helper'
require 'open-uri'

describe CorsLightController do
  describe 'GET #index' do
    context 'given a referrer and asset url' do
      let(:referrer_url) { 'http://referrer.url' }
      let(:asset_url) { 'http://asset.url' }
      let(:asset) { OpenStruct.new(read: 'Hello world!', content_type: 'text/plain') }

      it 'retrieves specified assets' do
        subject.expects(:open).with(asset_url).yields asset
        get :index, referrer_url: referrer_url, asset_url: asset_url
      end
    end

    context 'given a referrer and relative asset url' do
      let(:referrer_url) { 'http://referrer.url' }
      let(:asset_url) { 'http://asset.url/one/two/../three.txt' }
      let(:asset) { OpenStruct.new(read: 'Hello world!', content_type: 'text/plain') }

      it 'should retrieve the absolute asset' do
        subject.expects(:open).with('http://asset.url/one/three.txt').yields asset
        get :index, referrer_url: referrer_url, asset_url: asset_url
      end
    end

    context 'given a referrer and relative asset url with double slashes' do
      let(:referrer_url) { 'http://referrer.url' }
      let(:asset_url) { 'http://asset.url/one/two//..//three.txt' }
      let(:asset) { OpenStruct.new(read: 'Hello world!', content_type: 'text/plain') }

      it 'should retrieve the absolute asset' do
        subject.expects(:open).with('http://asset.url/one/three.txt').yields asset
        get :index, referrer_url: referrer_url, asset_url: asset_url
      end
    end

    context 'given a referrer and a an asset url with too many ../\'s' do
      let(:referrer_url) { 'http://referrer.url' }
      let(:asset_url) { 'http://asset.url/../../../..//three.txt' }
      let(:asset) { OpenStruct.new(read: 'Hello world!', content_type: 'text/plain') }

      it 'should retrieve the absolute asset' do
        subject.expects(:open).with('http://asset.url/three.txt').yields asset
        get :index, referrer_url: referrer_url, asset_url: asset_url
      end
    end

    context 'given a referrer and a an asset url with an empty proxy_whitelist' do
      let(:referrer_url) { 'http://referrer.url' }
      let(:asset_url) { 'http://asset.url' }
      let(:asset) { OpenStruct.new(read: 'Hello world!', content_type: 'text/plain') }

      it 'should retrieve the absolute asset' do
        subject.config.expects(:proxy_whitelist).returns([])
        subject.expects(:open).with(asset_url).yields asset
        get :index, referrer_url: referrer_url, asset_url: asset_url
      end
    end

    context 'given a referrer and a an asset url which are not whitelisted' do
      let(:referrer_url) { 'http://referrer.url' }
      let(:asset_url) { 'http://asset.url' }
      let(:asset) { OpenStruct.new(read: 'Hello world!', content_type: 'text/plain') }
      let(:domains) { ['http://notthatrefeerrerurl'] }

      it 'should not retrieve the absolute asset' do
        subject.config.expects(:proxy_whitelist).times(2).returns(domains)
        get :index, referrer_url: referrer_url, asset_url: asset_url

        response.status.should == 401
      end
    end

    context 'given a referrer and a an asset url which are whitelisted' do
      let(:referrer_url) { 'http://referrer.url' }
      let(:asset_url) { 'http://asset.url' }
      let(:asset) { OpenStruct.new(read: 'Hello world!', content_type: 'text/plain') }
      let(:domains) { [asset_url] }

      it 'should retrieve the absolute asset' do
        subject.config.expects(:proxy_whitelist).times(2).returns(domains)
        subject.expects(:open).with(asset_url).yields asset
        get :index, referrer_url: referrer_url, asset_url: asset_url
      end
    end

    context "when a 'proxy_read_timeout' has been set to zero seconds" do
      let(:read_timeout) { 2 }
      let(:referrer_url) { 'http://referrer.url' }
      let(:asset_url) { 'http://asset.url' }

      it 'OpenURI::OpenRead throws a ReadTimeout execption' do
        pending
        subject.config.expects(:proxy_read_timeout).returns(read_timeout)
        subject.stub!(:open).returns { raise Timeout::Error.new }

        lambda {
          get :index, referrer_url: referrer_url, asset_url: asset_url
        }.should raise_error Timeout::Error
      end
    end

    context "when a 'proxy_read_timeout' has been set to two seconds" do
      let(:read_timeout) { 2 }
      let(:referrer_url) { 'http://referrer.url' }
      let(:asset_url) { 'http://asset.url' }
      let(:asset) { OpenStruct.new(read: 'Hello world!', content_type: 'text/plain') }

      it 'calls OpenURI::OpenRead with a two second read_timeout' do
        pending
        subject.config.expects(:proxy_read_timeout).returns(read_timeout)
        subject.expects(:open).with(asset_url, read_timeout: read_timeout).yields asset
        Timeout.expects(:timeoutimeout).returns(read_timeout)

        get :index, referrer_url: referrer_url, asset_url: asset_url
      end
    end

  end

  describe '#realize_path' do

    let :url_with_no_path do
      'http://asset.url'
    end

    let :url_with_two_embedded_slashes do
      'http://asset.url/one/two//..//three.txt'
    end

    let :url_with_many_updirs do
      'http://asset.url/../../../..//three.txt'
    end

    let :url_with_two_beginning_slashes do
      'https://asset.url//assets/jquery-ui.js'
    end

    it 'does not modify a url without a path' do
      result = subject.send :realize_path, url_with_no_path
      result.should == 'http://asset.url'
    end

    it 'resolves embedded double slashes' do
      result = subject.send :realize_path, url_with_two_embedded_slashes
      result.should == 'http://asset.url/one/three.txt'
    end

    it 'resolves embedded updirs (../)' do
      result = subject.send :realize_path, url_with_many_updirs
      result.should == 'http://asset.url/three.txt'
    end

    it 'resolves double slashes that start a url' do
      result = subject.send :realize_path, url_with_two_beginning_slashes
      result.should == 'https://asset.url/assets/jquery-ui.js'
    end
  end

end
