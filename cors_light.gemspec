# CURRENT FILE :: cors_light.gemspec
require File.expand_path("../lib/cors_light/version", __FILE__)

# Provide a simple gemspec so that you can easily use your
# Enginex project in your Rails apps through Git.
Gem::Specification.new do |s|
  s.name                      = "cors_light"
  s.version                   = CorsLight::VERSION
  s.platform                  = Gem::Platform::RUBY
  s.authors                   = [ "Anthony Capone" ]
  s.email                     = [ "acapone@constantcontact.com" ]
  s.homepage                  = "http://constantcontact..com"
  s.description               = "Proxy and  add Cross Origin Resource Sharing headers to any request."
  s.summary                   = "cors_light-#{s.version}"

  s.rubyforge_project         = "cors_light"
  s.required_rubygems_version = "> 1.3.6"

  s.add_dependency "activesupport" , "~> 3.2.11"
  s.add_dependency "rails"         , "~> 3.2.11"

  s.files = Dir.glob("{bin,lib,app}/**/*") + %w(README.md)
  s.require_path = 'lib'
end
