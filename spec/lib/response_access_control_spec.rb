require_relative './../spec_helper'
require_relative './../../lib/response_access_control'
require 'action_controller'

describe 'ActionDispatch::Response' do

  context '#allow_referrer_access_control' do

    request = ActionDispatch::Request.new("rack.input" => "noideawhatthisisfor")
    request[:referrer_url] = 'http://localhost:80?bunny=thatkillssnakes'

    response = ActionDispatch::Response.new()
    response.expects(:request).at_least_once.returns request

    response.allow_referrer_access_control

    it 'should set the correct Access-Control-Allow headers' do
      response.headers['Access-Control-Allow-Origin'].should == 'http://localhost'
      response.headers['Access-Control-Allow-Methods'].should == 'GET'
      response.headers['Access-Control-Allow-Credentials'].should == 'true'
    end

  end

end

