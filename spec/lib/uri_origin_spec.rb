require 'spec_helper'
require 'open_uri_extensions'

describe 'URI::HTTP' do
  context '#origin' do

    it 'should return in format {scheme}://{host}:{port} if using a non-standard port' do
      uri = URI.parse('http://someurl.com:3000?param=thatshouldnotbeinorigin')
      uri.origin.should == 'http://someurl.com:3000'

      uri = URI.parse('https://someurl.com:3000?param=thatshouldnotbeinorigin')
      uri.origin.should == 'https://someurl.com:3000'
    end

    it 'should return in format {scheme}://{host} if using a standard port' do
      uri = URI.parse('http://someurl.com:80?param=thatshouldnotbeinorigin')
      uri.origin.should == 'http://someurl.com'

      uri = URI.parse('https://someurl.com:443?param=thatshouldnotbeinorigin')
      uri.origin.should == 'https://someurl.com'
    end

    it 'should return in format {scheme}://{host} if using the default port' do
      uri = URI.parse('http://someurl.com?param=thatshouldnotbeinorigin')
      uri.origin.should == 'http://someurl.com'

      uri = URI.parse('https://someurl.com?param=thatshouldnotbeinorigin')
      uri.origin.should == 'https://someurl.com'
    end
  end
end
