module CorsLight
  class ProxyController < ActionController::Base
    require 'open-uri'
    require 'open_uri_extensions'
    require 'response_access_control'
    require 'openssl'

    Kernel.silence_warnings { OpenSSL::SSL::VERIFY_PEER = OpenSSL::SSL::VERIFY_NONE }

    def index
      unless params[:referrer_url] && !params[:referrer_url].empty? &&
             params[:asset_url] && !params[:asset_url].empty?
        raise ActionController::RoutingError.new('Not Found')
      end

      response.allow_referrer_access_control

      download_started_at = Time.now
      read_asset realize_path params[:asset_url] do |asset_response|
        asset = asset_response.read
        asset = encode64_data_url(asset) if params[:format] == 'data'
        log_proxy_time download_started_at, Time.now
        send_data asset, type: response_content_type(asset_response), disposition: 'inline'
      end
    end

    private

    def read_asset(url)
      render(json: 'Invalid Domain', status: 401) and return if !valid_domain?(url)

      if !config.proxy_local_assets and local_asset?(url)
        yield read_local_asset(url)
      else
        begin
          Timeout::timeout(config.proxy_read_timeout) {
            open URI.encode(url) do |response|
              yield response
            end
          }
        rescue OpenURI::HTTPError => error
          not_found(error) if error.io.status.first == '404'
          raise "Error retrieving asset from #{params[:asset_url]}. Internal Exception: #{error.message}"
        rescue Timeout::Error => error
          raise "Timeout retrieving asset from #{params[:asset_url]}. Internal Exception: #{error.message}"
        end
      end
    end

    def valid_domain?(url)
      raise ActionController::RoutingError.new("invalid url #{url}") unless url =~ /^https?:\/\//

      config.proxy_whitelist.blank? or config.proxy_whitelist.any? { |domain| url.start_with? domain }
    end

    def read_local_asset(url)
      asset = find_asset_from_url url
      OpenStruct.new({ read: asset.body, content_type: asset.content_type })
    end

    def local_asset?(url)
      asset_url = URI.strip_protocol url

      raise "No application url specified.
             Please configure 'Rails.configuration.application_url'" if Rails.configuration.application_url.to_s == ''

      asset_url.start_with? URI.strip_protocol(Rails.configuration.application_url)
    end

    def find_asset_from_url(url)
      asset_path = URI.parse(url).path.gsub(/^.+assets\//, '')
      asset = Rails.application.assets.find_asset asset_path
      raise ActionController::RoutingError.new("local asset not found #{asset_path}") if asset.nil?
      asset
    end

    def realize_path(url)
      URI.realize_path url
    rescue URI::InvalidURIError => error
      not_found error
    end

    def not_found(error)
      raise ActionController::RoutingError.new(error.message)
    end

    def response_content_type(asset_response)
      params[:format] == 'data' ? 'text/html' : asset_response.content_type
    end

    def encode64_data_url(asset)
      "data:#{asset_response.content_type};base64,#{Base64.encode64(asset)}"
    end

    def log_proxy_time(start_time, end_time)
      logger.debug "Cors Light took #{end_time - start_time} seconds to proxy #{params[:asset_url]} for #{params[:referrer_url]}"
    end
  end
end
